#!/bin/bash

# shellcheck disable=SC2155
export SOURCE_DATE_EPOCH=$(git log -1 --format=%ct)

export STEAM_APP_ID_LIST="336130 307580 307570 212050"

source lib.sh
